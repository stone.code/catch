package catch_test

import (
	"errors"
	"fmt"
	"os"

	"gitlab.com/stone.code/catch"
	testing "gitlab.com/stone.code/catch/internal/mock"
)

func ExampleCheck() {
	testing.Run("Example", func(t *testing.T) {
		// Test setup goes here.
		// ...
		fancyResult := 1 + 1

		// Make sure that the result is expected.
		catch.Check(t, fancyResult == 2)
	})
}

func ExampleCheck_fail() {
	testing.Run("Example", func(t *testing.T) {
		// Test setup goes here.
		// ...
		fancyResult := 1 + 1

		// Make sure that the result is expected.
		catch.Check(t, fancyResult == 3)
	})

	// Output:
	// --- FAIL: Example (0.00s)
	//     example.go:99: check failed: fancyResult == 3
}

func ExampleCheck_annotations() {
	// This example shows the two ways to annotate a test with extra
	// information.  First, extra parameters are included in the error message
	// if the test fails.  Second, line comments after the closing parenthesis
	// are also included in the error message.
	testing.Run("Example", func(t *testing.T) {
		const message = "A message"
		const value = 1234

		// Test setup goes here.
		// ...
		fancyResult := 1 + 1

		// Make sure that the result is expected.
		catch.Check(t, fancyResult == 3, message, value) // Comment that explains the test.
	})

	// Output:
	// --- FAIL: Example (0.00s)
	//     example.go:99: check failed: fancyResult == 3
	//         message  : A message
	//         value    : 1234
	//         comment  : Comment that explains the test.
}

func ExampleCheckEqual() {
	testing.Run("Example", func(t *testing.T) {
		// Test setup goes here.
		// ...
		fancyResult := 1 + 1

		// Make sure that the result is expected.
		catch.CheckEqual(t, fancyResult, 2)
	})
}

func ExampleCheckEqual_fail() {
	testing.Run("Example", func(t *testing.T) {
		// Test setup goes here.
		// ...
		fancyResult := 1 + 1

		// Make sure that the result is expected.
		catch.CheckEqual(t, fancyResult, 3)
	})

	// Output:
	// --- FAIL: Example (0.00s)
	//     example.go:99: check failed: fancyResult == 3
	//         got      : 2
	//         expected : 3
}

func ExampleCheckEqual_err() {
	testing.Run("Example", func(t *testing.T) {
		// Test setup goes here.
		// ...
		err := errors.New("custom error")

		// Make sure that the result is expected.
		catch.CheckEqual(t, err, nil)
	})

	// Output:
	// --- FAIL: Example (0.00s)
	//     example.go:99: check failed: err == nil
	//         got      : custom error
	//         expected : <nil>
}

func ExampleCheckIsNil() {
	testing.Run("Example", func(t *testing.T) {
		// Test setup goes here.
		// ...
		var s []byte

		// Make sure that the result is expected.
		catch.CheckIsNil(t, s)
	})
}

func ExampleCheckIsNil_fail() {
	testing.Run("Example", func(t *testing.T) {
		// Test setup goes here.
		// ...
		s := make([]byte, 0)

		// Make sure that the result is expected.
		catch.CheckIsNil(t, s)
	})

	// Output:
	// --- FAIL: Example (0.00s)
	//     example.go:99: check is nil failed: s
	//         got      : []
}

func ExampleRequire() {
	testing.Run("Example", func(t *testing.T) {
		// Open file with test data.  Abort the test if the file cannot be
		// opened.
		file, err := os.Open("required-test-data.txt")
		catch.Require(t, err == nil)
		// The error must be nil.  Otherwise, test would have been aborted by the
		// previous call to Require would have aborted the test.
		defer file.Close()

		// Count the number of lines in the file.
		fancyResult := 1 + 1

		// Make sure that the result is expected.
		catch.Require(t, fancyResult == 2)
	})
}

func ExampleRequire_fail() {
	testing.Run("Example", func(t *testing.T) {
		// Perform terribly complicated calculations.
		fancyResult := 1 + 2

		// Make sure that the result is expected.
		catch.Require(t, fancyResult == 2)

		// This will never print, as the test will be aborted.
		fmt.Println("done.")
	})

	// Output:
	// --- FAIL: Example (0.00s)
	//     example.go:99: require failed: fancyResult == 2
}
