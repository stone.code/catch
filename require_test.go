package catch_test

import (
	"errors"
	"testing"

	"gitlab.com/stone.code/catch"
	"gitlab.com/stone.code/catch/internal/mock"
)

func TestRequire(t *testing.T) {
	t.Run("true", func(t *testing.T) {
		mock := mock.NewT()
		catch.Require(mock, 1+1 == 2)

		mock.CheckSucceeded(t)

		if got := mock.Text(); got != "" {
			t.Errorf("got %s", got)
		}
	})

	t.Run("false", func(t *testing.T) {
		mock := mock.NewT()
		catch.Require(mock, 1+2 == 2)

		mock.CheckFailed(t, true)

		const expected = "require failed: 1+2 == 2"
		if got := mock.Text(); got != expected {
			t.Errorf("expected %s, got %s", expected, got)
		}
	})
}

func TestRequireEqual(t *testing.T) {
	t.Run("true", func(t *testing.T) {
		mock := mock.NewT()
		catch.RequireEqual(mock, 2, 1+1)

		mock.CheckSucceeded(t)

		if got := mock.Text(); got != "" {
			t.Errorf("got %s", got)
		}
	})

	t.Run("false", func(t *testing.T) {
		mock := mock.NewT()
		catch.RequireEqual(mock, 2, 1+2)

		mock.CheckFailed(t, true)

		const expected = `require failed: 2 == 1 + 2
got      : 2
expected : 3`
		if got := mock.Text(); got != expected {
			t.Errorf("expected %s, got %s", expected, got)
		}
	})

	t.Run("no error", func(t *testing.T) {
		mock := mock.NewT()
		catch.RequireEqual(mock, errors.New("mock error"), nil)

		mock.CheckFailed(t, true)

		const expected = `require failed: errors.New("mock error") == nil
got      : mock error
expected : <nil>`
		if got := mock.Text(); got != expected {
			t.Errorf("expected %s, got %s", expected, got)
		}
	})

	// RequireEqual behaviour on inequality.
	// Unprintable characters
	t.Run("\"\x1b[0m\" == \"\x1b[1m\"", func(t *testing.T) {
		mock := mock.NewT()
		catch.RequireEqual(mock, "\x1b[0m", "\x1b[1m")

		mock.CheckFailed(t, true)

		const expected = `require failed: "\x1b[0m" == "\x1b[1m"
got      : "\x1b[0m"
expected : "\x1b[1m"`
		if got := mock.Text(); got != expected {
			t.Errorf("expected %q, got %q", expected, got)
		}
	})

	// RequireEqual behaviour on inequality.
	// Mismatchted types
	t.Run("1 == uint(1)", func(t *testing.T) {
		mock := mock.NewT()
		catch.RequireEqual(mock, 1, uint(1))

		mock.CheckFailed(t, true)

		const expected = `require failed: 1 == uint(1)
got      : 1
expected : 1
types    : int != uint`
		if got := mock.Text(); got != expected {
			t.Errorf("expected %s, got %s", expected, got)
		}
	})
}

func TestRequireIsNil(t *testing.T) {
	// Check behaviour on true.
	t.Run("true", func(t *testing.T) {
		mock := mock.NewT()
		catch.RequireIsNil(mock, ([]byte)(nil))

		mock.CheckSucceeded(t)

		if got := mock.Text(); got != "" {
			t.Errorf("got %#v", got)
		}
	})

	// Check behaviour on false.
	t.Run("false", func(t *testing.T) {
		mock := mock.NewT()
		catch.RequireIsNil(mock, []byte{})

		mock.CheckFailed(t, true)

		const expected = "require is nil failed: []byte{}\ngot      : []"
		if got := mock.Text(); got != expected {
			t.Errorf("expected %#v, got %#v", expected, got)
		}
	})

	// Check behaviour on false.
	// Source code includes a comment
	t.Run("comment", func(t *testing.T) {
		mock := mock.NewT()
		catch.RequireIsNil(mock, []byte{}) // My comment

		mock.CheckFailed(t, true)

		const expected = "require is nil failed: []byte{}\ngot      : []\ncomment  : My comment"
		if got := mock.Text(); got != expected {
			t.Errorf("expected %#v, got %#v", expected, got)
		}
	})

	// Check behaviour on false.
	// Slice contains bytes
	t.Run("printable", func(t *testing.T) {
		mock := mock.NewT()
		catch.RequireIsNil(mock, []byte{1, 2, 3})

		mock.CheckFailed(t, true)

		const expected = "require is nil failed: []byte{1, 2, 3}\ngot      : [1 2 3]"
		if got := mock.Text(); got != expected {
			t.Errorf("expected %#v, got %#v", expected, got)
		}
	})

	// Check behaviour on false.
	// Slice contains strings.
	t.Run("unprintable", func(t *testing.T) {
		mock := mock.NewT()
		testSlice := []string{"Hello", ",", "world!"}
		catch.RequireIsNil(mock, testSlice)

		mock.CheckFailed(t, true)

		const expected = "require is nil failed: testSlice\ngot      : []string{\"Hello\", \",\", \"world!\"}"
		if got := mock.Text(); got != expected {
			t.Errorf("expected %#v, got %#v", expected, got)
		}
	})
}
