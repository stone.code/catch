# ![logo](./assets/logo32.png) Package catch
> Testing with better error messages and less code.

Package catch provides utility functions that work with the standard library's testing capabilities. The functions encapsulate standard tests, and standardize logging for failures.  However, package catch does not output anything directly.  All messages are formatted using the standard methods on the test object.

* Simple, lean API that handles the formatting of all error messages.
* Does not replace `*testing.T`, and can be mixed freely with other test code using the standard library.
* Easily annotate tests with comments or additional fields.

As an example, when the following test fails:

    func TestFancyCalculation(t *testing.T) {
        fancyCalculation := 1 + 1
        catch.CheckEqual(t, fancyCalculation, 1)
    }

The log will be:

    example_test.go:99: check failed: fancyCalculation == 1
            expected : 1
            got      : 2

The source files for the test code should be available and unmodified at their original locations when the tests are run.  The source code is read and parsed to create messages for any failed tests.  If the source code is not available, tests can still be run, but the messages for any failed tests will not be complete.

## Install

Go version 1.9 or later is required.  The package can be installed from the command line using the [go](https://golang.org/cmd/go/) tool.

    go get gitlab.com/stone.code/catch

This package will not work with go version 1.8 or earlier.  It depends on the method Helper for [`testing.B`](https://pkg.go.dev/testing#B.Helper) and [`testing.T`](https://pkg.go.dev/testing#T.Helper) to properly format messages.

## Getting Started

Package documentation and examples are on [godoc](https://pkg.go.dev/gitlab.com/stone.code/catch).

## Contribute

Feedback and PRs welcome.  You can also [submit an issue](https://gitlab.com/stone.code/catch/issues).

This package is considered to be complete, but additional checks may be considered.  The main criteria for a new function will be if it can provide a better message when the test fails.  However, other types of utility functions are probably out-of-scope.

[![Go Report Card](https://goreportcard.com/badge/gitlab.com/stone.code/catch)](https://goreportcard.com/report/gitlab.com/stone.code/catch)

### Benchmarking

While performance is not a primary concern, the performance for generating messages is monitored.  Currently, logging with this package does not incur significant overhead compared with hand-written calls to `testing.T.Logf` and family.  In addition to the standard `go test`, please verify the benchmarks for any changes.

## Related Projects

* [Catch2](https://github.com/catchorg/Catch2):  A modern, C++-native, header-only, test framework for unit-tests, TDD and BDD.
* [github.com/frankban/quicktest](https://pkg.go.dev/github.com/frankban/quicktest):  Package quicktest provides a collection of Go helpers for writing tests.
* [github.com/google/go-cmp/cmp](https://pkg.go.dev/github.com/google/go-cmp/cmp):  Package for equality of Go values.
* [github.com/matryer/is](https://pkg.go.dev/github.com/matryer/is):  Package is provides a lightweight extension to the standard library's testing capabilities.
* [github.com/stretchr/testify](https://pkg.go.dev/github.com/stretchr/testify):  Package testify is a set of packages that provide many tools for testifying that your code will behave as you intend.
* [gopkg.in/check.v1](https://pkg.go.dev/gopkg.in/check.v1):  Package check is a rich testing extension for Go's testing package.
* [github.com/alecthomas/assert](https://github.com/alecthomas/assert):  A simple assertion library using Go generics.
* [gotest.tools/v3/assert](https://pkg.go.dev/gotest.tools/v3/assert):  Package assert provides assertions for comparing expected values to actual values in tests. When an assertion fails a helpful error message is printed.
* [https://pkg.go.dev/github.com/carlmjohnson/be](https://pkg.go.dev/github.com/carlmjohnson/be):  Package be is the minimalist testing helper for Go.

## License

BSD (c) Robert Johnstone
