package catch

import (
	"bytes"
	"errors"
	"go/ast"
	"go/printer"
	"go/token"
	"runtime"
	"strings"

	"gitlab.com/stone.code/catch/internal/cache"
	"gitlab.com/stone.code/catch/internal/polyfill"
)

const (
	// This string is used when there is an error loading the expression or
	// expressions from source.
	unknownExpression = "????"
)

func splitExpression1(expr []string) (expr1 string, rest []string) {
	if len(expr) >= 1 {
		return expr[0], expr[1:]
	}

	return unknownExpression, nil
}

func splitExpression2(expr []string) (expr1, expr2 string, rest []string) {
	if len(expr) >= 2 {
		return expr[0], expr[1], expr[2:]
	}

	return unknownExpression, unknownExpression, nil
}

func loadExpressionFromCaller() ([]string, string, error) {
	// Find the source location for the check call
	_, path, line, ok := runtime.Caller(2)
	if !ok {
		panic("gitlab.com/stone.code/assert/check: could not identify caller")
	}

	return loadExpressionFromFile(path, line)
}

func loadExpressionFromFile(path string, line int) ([]string, string, error) {
	fset, file, err := cache.LoadFile(path)
	if err != nil {
		return nil, "", err
	}

	if lineCount := fset.File(file.Pos()).LineCount(); line > lineCount {
		return nil, "", errors.New("invalid line number")
	}

	visitor := &callExprVisitor{
		LinePos: polyfill.LineStart(fset.File(file.Pos()), line),
		LineEnd: polyfill.LineStart(fset.File(file.Pos()), line+1),
	}
	ast.Walk(visitor, file)
	if len(visitor.Args) < 2 {
		return nil, "", errors.New("incorrect call expressions identified")
	}
	visitor.Args = visitor.Args[1:]

	exprs := make([]string, 0, len(visitor.Args))
	for _, v := range visitor.Args {
		// Convert the expression to a string.
		//
		// Calls to printer.Fprint are fairly expensive.  In the common case
		// that the expression is an identifier, the result will just be the
		// identifier's name, so short-circuit the expensive machinery in
		// printer.Fprint.
		if ident, ok := v.(*ast.Ident); ok {
			exprs = append(exprs, ident.Name)
		} else {
			out := bytes.NewBuffer(nil)
			printer.Fprint(out, fset, v)
			exprs = append(exprs, out.String())
		}
	}

	comment := ""
	commentLine := fset.Position(visitor.Rparen).Line
	for _, v := range file.Comments {
		for _, u := range v.List {
			if u.Slash > visitor.Rparen && fset.Position(u.Slash).Line == commentLine {
				comment = strings.TrimSpace(u.Text[2:])
			}
		}
	}

	return exprs, comment, nil
}

type callExprVisitor struct {
	LinePos token.Pos
	LineEnd token.Pos
	Args    []ast.Expr
	Rparen  token.Pos
}

func (v *callExprVisitor) Visit(node ast.Node) ast.Visitor {
	if node == nil {
		return nil
	}

	if callexpr, ok := node.(*ast.CallExpr); ok {
		// If the call expression is on the line, then this is the target. Note,
		// we should only be looking for call expressions without a return
		// value, so the target will always be the outermost call expression on
		// any line.
		if node.Pos() >= v.LinePos && node.Pos() < v.LineEnd {
			v.Args = callexpr.Args
			v.Rparen = callexpr.Rparen

			return nil
		}
	}

	if node.Pos() < v.LineEnd && node.End() > v.LinePos {
		return v
	}

	return nil
}
