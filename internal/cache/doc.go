// Package cache maintains a cache of parsed Go source files.
//
// Loading and parsing a source file is relatively expensive to print an error
// message.  This package keeps the AST for any parsed files in memory.
//
// The need for the cache to have global lifetime requires package-level
// variables.  Those variables are isolated in this package to limit visibility.
package cache
