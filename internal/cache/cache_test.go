package cache_test

import (
	"testing"

	"gitlab.com/stone.code/catch/internal/cache"
)

func TestLoadFile(t *testing.T) {
	// Load the file.
	fset, file, err := cache.LoadFile("./cache.go")
	if fset == nil {
		t.Errorf("fset should not be nil")
	}
	if file == nil {
		t.Errorf("file should not be nil")
	}
	if err != nil {
		t.Errorf("err should be nil")
	}

	// Reload the file
	fset2, file2, err := cache.LoadFile("./cache.go")
	if fset2 != fset {
		t.Errorf("fset should be the same")
	}
	if file2 != file {
		t.Errorf("file should be the same")
	}
	if err != nil {
		t.Errorf("err should be nil")
	}

	_, _, err = cache.LoadFile("./dne.go")
	if err == nil {
		t.Errorf("err should not be nil")
	}
}
