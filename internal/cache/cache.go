package cache

import (
	"go/ast"
	"go/parser"
	"go/token"
	"sync"
)

var (
	mu    sync.Mutex
	fset  *token.FileSet
	files map[string]*ast.File
)

func LoadFile(path string) (*token.FileSet, *ast.File, error) {
	mu.Lock()
	defer mu.Unlock()

	if files != nil {
		if file, ok := files[path]; ok {
			return fset, file, nil
		}
	} else {
		fset = token.NewFileSet()
		files = map[string]*ast.File{}
	}

	file, err := parser.ParseFile(fset, path, nil, parser.ParseComments)
	if err != nil {
		return nil, nil, err
	}
	files[path] = file

	return fset, file, nil
}
