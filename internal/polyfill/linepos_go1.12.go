//go:build go1.12
// +build go1.12

package polyfill

import (
	"go/token"
)

// LineStart is an indirect to token.File.LineStart, to support a polyfill on
// versions of Go before 1.12.
func LineStart(file *token.File, line int) token.Pos {
	return file.LineStart(line)
}
