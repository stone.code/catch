// Package polyfill supplies a polyfill for token.File.LineStart, introduced
// with Go version 1.12.
package polyfill
