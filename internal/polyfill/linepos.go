//go:build !go1.12
// +build !go1.12

package polyfill

import (
	"go/token"
)

// LineStart is a polyfill of token.File.LineStart, which was introduced in Go 1.12.
func LineStart(file *token.File, line int) token.Pos {
	a := file.Pos(0)
	b := file.Pos(file.Size())

	for file.Line(a) != line {
		c := (a + b) / 2

		if file.Line(c) <= line {
			a = c
		} else {
			b = c
		}
	}

	for a > file.Pos(0) && file.Line(a-1) == line {
		a--
	}

	return a
}
