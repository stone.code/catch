package mock

import (
	"bytes"
	"fmt"
	"strings"
	"testing"
)

// T is a type passed to Test functions to manage test state and support formatted test logs.
// A minimal mock of testing.T.
type T struct {
	panicOnFatal bool

	failed bool
	fatal  bool
	buffer *bytes.Buffer
}

// CheckSucceeded verifies that a mock test succeeded, otherwise it logs the
// failure.
func (m *T) CheckSucceeded(t *testing.T) {
	t.Helper()

	if m.failed || m.fatal {
		t.Errorf("test unexpectedly failed")
	}
}

// CheckFailed verifies that a mock test failed, otherwise it logs the
// test failure.
func (m *T) CheckFailed(t *testing.T, fatal bool) {
	t.Helper()

	if !m.failed {
		t.Errorf("test unexpectedly succeeded")
	}

	if fatal != m.fatal {
		if m.fatal {
			t.Errorf("test unexpectedly marked as fatal")
		} else {
			t.Errorf("test unexpectedly not marked as fatal")
		}
	}
}

// Errorf is equivalent to Logf followed by Fail.
func (m *T) Errorf(format string, args ...interface{}) {
	if m.fatal {
		panic("Already seen a fatal error")
	}

	m.Logf(format, args...)
	m.Fail()
}

// Fatalf is equivalent to Logf followed by FailNow.
func (m *T) Fatalf(format string, args ...interface{}) {
	if m.fatal {
		panic("Already seen a fatal error")
	}

	m.Logf(format, args...)
	m.FailNow()
}

// Fail marks the function as having failed but continues execution.
func (m *T) Fail() {
	m.failed = true
}

// FailNow marks the function as having failed.
//
// Depending on the mode, it may either terminate further tests by panicking
// or it may not.  To get termination, use the function Run.  Otherwise,
// fatal errors are simply recorded.
func (m *T) FailNow() {
	m.failed = true
	m.fatal = true

	if m.panicOnFatal {
		panic(ErrFailNow{})
	}
}

// Helper marks the calling function as a test helper function.
// This is a no-op for the mock.
func (m *T) Helper() {
	// do nothing
}

// Logf formats its arguments according to the format, analogous to Printf,
// and records the text in the error log.
func (m *T) Logf(format string, args ...interface{}) {
	if m.fatal {
		panic("Already seen a fatal error")
	}

	if m.buffer.Len() > 0 {
		m.buffer.WriteByte('\n')
	}
	fmt.Fprintf(m.buffer, format, args...)
}

func (m *T) print() {
	if m.failed {
		fmt.Printf("--- FAIL: Example (0.00s)\n    example.go:99: %s",
			strings.Replace(m.Text(), "\n", "\n        ", -1))
	}
}

// Reset returns the state of the test object to as if it was just initialized.
func (m *T) Reset() {
	m.failed = false
	m.fatal = false
	m.buffer.Reset()
}

// Text returns the error log for the test.
func (m *T) Text() string {
	return m.buffer.String()
}

// NewT returns a newly constructed T.
func NewT() *T {
	return &T{
		failed: false,
		buffer: bytes.NewBuffer(nil),
	}
}

// Run initializes an environment that mimics a test function.
func Run(_ string, f func(*T)) {
	m := NewT()
	m.panicOnFatal = true
	defer func() {
		if r := recover(); r != nil {
			if _, ok := r.(ErrFailNow); !ok {
				panic(r)
			}
			m.print()
		}
	}()

	f(m)

	m.print()
}
