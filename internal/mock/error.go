package mock

// ErrFailNow is the type used in panics to abort a test when users call
// FailNow.
type ErrFailNow struct{}
