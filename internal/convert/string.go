package convert

import "fmt"

// AsString returns the string representation of v, either because it is
// already a string, implements the fmt.Stringer interface, or implements
// the error interface.
func AsString(v interface{}) (s string, ok bool) {
	if s, ok := v.(string); ok {
		return s, true
	}

	if s, ok := v.(fmt.Stringer); ok {
		return s.String(), true
	}

	if s, ok := v.(error); ok {
		return s.Error(), true
	}

	return "", false
}
