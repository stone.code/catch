package convert_test

import (
	"fmt"
	"testing"

	"gitlab.com/stone.code/catch/internal/convert"
)

type myStruct1 struct{}

type myStruct2 struct{}

func (myStruct2) String() string {
	return "myStruct2"
}

type myStruct3 struct{}

func (myStruct3) Error() string {
	return "myStruct3"
}

func TestAsString(t *testing.T) {
	cases := []struct {
		in  interface{}
		out string
		ok  bool
	}{
		{nil, "", false},
		{"string", "string", true},
		{myStruct1{}, "", false},
		{myStruct2{}, "myStruct2", true},
		{myStruct3{}, "myStruct3", true},
	}

	for _, v := range cases {
		t.Run(fmt.Sprintf("%#v", v.in), func(t *testing.T) {
			out, ok := convert.AsString(v.in)

			if out != v.out {
				t.Errorf("want %#v, got %#v", v.out, out)
			}

			if ok != v.ok {
				t.Errorf("want %#v, got %#v", v.ok, ok)
			}
		})
	}
}
