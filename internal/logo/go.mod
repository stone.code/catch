module gitlab.com/stone.code/catch/internal/logo

go 1.17

require (
	github.com/golang/freetype v0.0.0-20170609003504-e2365dfdc4a0 // indirect
	github.com/llgcode/draw2d v0.0.0-20210904075650-80aa0a2a901d
	golang.org/x/image v0.0.0-20210628002857-a66eb6448b8d // indirect
)
