package main

import (
	"image"
	"image/color"
	"math"

	"github.com/llgcode/draw2d"
	"github.com/llgcode/draw2d/draw2dimg"
)

var (
	// Define our colours
	black  = color.RGBA{0, 0, 0, 0xff}
	grey   = color.RGBA{192, 192, 192, 0xff}
	white  = color.RGBA{0xff, 0xff, 0xff, 0xff}
	accent = color.RGBA{34, 139, 34, 0xff}

	// Define our font
	fontData = draw2d.FontData{
		Name:   "Go-Mono",
		Family: draw2d.FontFamilySerif,
		Style:  draw2d.FontStyleBold,
	}
)

func main() {
	avatar32()
	avatar64()
	avatar128()
}

func avatar32() {
	// Initialize the graphic context on an RGBA image
	dest := image.NewRGBA(image.Rect(0, 0, 32, 32))
	gc := draw2dimg.NewGraphicContext(dest)

	drawG(gc, 16, 16, 14, 2)
	draw2dimg.SaveToPngFile("avatar32.png", dest)
}

func avatar64() {
	// Initialize the graphic context on an RGBA image
	dest := image.NewRGBA(image.Rect(0, 0, 64, 64))
	gc := draw2dimg.NewGraphicContext(dest)

	drawG(gc, 32, 32, 30, 4)
	draw2dimg.SaveToPngFile("avatar64.png", dest)
}

func avatar128() {
	// Initialize the graphic context on an RGBA image
	dest := image.NewRGBA(image.Rect(0, 0, 128, 128))
	gc := draw2dimg.NewGraphicContext(dest)

	drawG(gc, 62, 62, 60, 8)
	draw2dimg.SaveToPngFile("avatar128.png", dest)
}

func drawG(gc draw2d.GraphicContext, x, y, r, sw float64) {
	println("G:", sw/r)

	// White fill for interior
	gc.SetFillColor(white)
	gc.MoveTo(x, 0)
	gc.ArcTo(x, y, r, r, 0, 2*math.Pi)
	gc.Close()
	gc.Fill()

	// Grey accent sectors
	gc.SetFillColor(grey)
	gc.MoveTo(x-sw, 0)
	gc.ArcTo(x, y, r-sw, r-sw, 0, 2*math.Pi)
	gc.Close()
	gc.Fill()

	// Clear grey from region for text
	gc.SetFillColor(white)
	gc.MoveTo(x+r+sw, y+r*math.Sin(math.Pi/6)-sw/2)
	gc.LineTo(x-r, y+r*math.Sin(math.Pi/6)-sw/2)
	gc.LineTo(x-r, y+r*math.Sin(-math.Pi/6)+sw/2)
	gc.LineTo(x+r+sw, y+r*math.Sin(-math.Pi/6)+sw/2)
	gc.Close()
	gc.Fill()

	// Stylized border
	gc.SetStrokeColor(accent)
	gc.SetLineWidth(sw)
	gc.BeginPath()
	gc.MoveTo(x+r+sw/2, y+r*math.Sin(math.Pi/6))
	gc.ArcTo(x, y, r, r, math.Pi/6, math.Pi*5/3)
	gc.LineTo(x+r+sw/2, y+r*math.Sin(-math.Pi/6))
	gc.Stroke()

	// Clear green from region for text
	gc.SetFillColor(color.RGBA{0x80, 0x80, 0x80, 0x80})
	gc.MoveTo(x+r+sw, y+r*math.Sin(math.Pi/6)-sw/2)
	gc.LineTo(x-r-sw, y+r*math.Sin(math.Pi/6)-sw/2)
	gc.LineTo(x-r-sw, y+r*math.Sin(-math.Pi/6)+sw/2)
	gc.LineTo(x+r+sw, y+r*math.Sin(-math.Pi/6)+sw/2)
	gc.Close()
	gc.Fill()

	// Logo text
	draw2d.SetFontFolder(".")
	draw2d.SetFontNamer(func(fd draw2d.FontData) string {
		// Force the use of our selected font file.
		return "Go-Bold.ttf"
	})

	gc.SetFontData(fontData)
	gc.SetFontSize(r / 2.1) // match stroke width to G
	gc.SetFillColor(black)
	gc.FillStringAt("CATCH", x+centerString(gc, "CATCH"), y+centerStringY(gc, "CATCH"))
}

func centerString(gc draw2d.GraphicContext, s string) float64 {
	left, _, right, _ := gc.GetStringBounds(s)
	return -(left + right) / 2
}

func centerStringY(gc draw2d.GraphicContext, s string) float64 {
	_, top, _, bottom := gc.GetStringBounds(s)
	return -(top + bottom) / 2
}
