package catch

import (
	"reflect"
	"strings"
)

// Require does nothing if 'value' is true, but will a report an error if it is
// false.  The error is reported using Fatalf, meaning that the error will be
// logged and the test will fail immediately.
func Require(t TB, value bool, fields ...interface{}) {
	// If the check passes, there is nothing to report
	if value {
		return
	}

	// Skip this function when logging the location of the error.
	t.Helper()

	// Load the call expression that failed.
	expr, comment, err := loadExpressionFromCaller()
	must(t, err)

	// Format the message.
	expr1, rest := splitExpression1(expr)
	t.Fatalf("require failed: %s%s%s", expr1,
		formatFields(rest, fields),
		formatComment(comment))
}

// RequireEqual does nothing if 'expected' and 'got' are equal, but will a
// report an error otherwise.  The error is reported using Fatalf, meaning
// that the error will be logged and the test will fail immediately.
func RequireEqual(t TB, got, expected interface{}, fields ...interface{}) {
	// If the check passes, there is nothing to report
	if reflect.DeepEqual(got, expected) {
		return
	}

	// Skip this function when logging the location of the error.
	t.Helper()

	const msg = `require failed: %s == %s
got      : %v
expected : %v%s%s%s`

	// Load the call expression that failed.
	expr, comment, err := loadExpressionFromCaller()
	must(t, err)

	// Format the message.
	expr1, expr2, rest := splitExpression2(expr)
	if isPrintable(got) && isPrintable(expected) {
		t.Fatalf(msg, expr1, expr2, got, expected,
			formatTypes(got, expected),
			formatFields(rest, fields),
			formatComment(comment))
	} else {
		t.Fatalf(strings.Replace(msg, "%v", "%#v", -1),
			expr1, expr2, got, expected,
			formatTypes(got, expected),
			formatFields(rest, fields),
			formatComment(comment))
	}
}

// RequireIsNil does nothing if 'got' is a nil chan, func, map,
// pointer, or slice value, but will a report an error otherwise.  The error is
// reported using Fatalf, meaning that the error will be logged and the test
// will fail immediately.
//
// This function can only be called with chan, func, map, pointer, or slice
// values.  It will panic otherwise.
func RequireIsNil(t TB, got interface{}, fields ...interface{}) {
	// If the check passes, there is nothing to report.
	if reflect.ValueOf(got).IsNil() {
		return
	}

	// Skip this function when logging the location of the error.
	t.Helper()

	const msg = `require is nil failed: %s
got      : %v%s%s`

	// Load the call expression that failed.
	expr, comment, err := loadExpressionFromCaller()
	must(t, err)

	// Format the message.
	expr1, rest := splitExpression1(expr)
	if isPrintable(got) {
		t.Fatalf(msg, expr1, got,
			formatFields(rest, fields),
			formatComment(comment))
	} else {
		t.Fatalf(strings.Replace(msg, "%v", "%#v", -1),
			expr1, got,
			formatFields(rest, fields),
			formatComment(comment))
	}
}
