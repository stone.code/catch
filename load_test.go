package catch

import (
	"strconv"
	"testing"
)

func TestLoadExpressionFromFile(t *testing.T) {
	// Self-check
	expr, _, err := loadExpressionFromFile("./load_test.go", 11)
	Check(t, err == nil)
	CheckEqual(t, expr, []string{"err == nil"})

	cases := []struct {
		path string
		line int
		expr []string
		ok   bool
	}{
		{"./load_test.go", 11, []string{"err == nil"}, true},
		{".does.not.exist", 11, nil, false},
		{"./load_test.go", 1e6, nil, false}, // Line does not exist
		{"./load_test.go", 3, nil, false},   // Start of imports
		{"./load_test.go", 4, nil, false},   // import line
		{"./load_test.go", 6, nil, false},   // End of imports
		{"./load_test.go", 7, nil, false},   // Blank line
		{"./load_test.go", 8, nil, false},   // Function call, but only one parameter
		{"./load_test.go", 9, nil, false},   // Comment line
	}

	for i, v := range cases {
		t.Run(strconv.Itoa(i), func(t *testing.T) {
			expr, _, err := loadExpressionFromFile(v.path, v.line)
			CheckEqual(t, expr, v.expr)
			CheckEqual(t, err == nil, v.ok)
		})
	}
}
