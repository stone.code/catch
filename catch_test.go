package catch_test

import (
	"testing"

	"gitlab.com/stone.code/catch"
)

var (
	_ catch.TB = (*testing.T)(nil)
	_ catch.TB = (*testing.B)(nil)
	_ catch.TB = (testing.TB)(nil)
)
