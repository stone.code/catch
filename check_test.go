package catch_test

import (
	"errors"
	"testing"

	"gitlab.com/stone.code/catch"
	"gitlab.com/stone.code/catch/internal/mock"
)

func TestCheck(t *testing.T) {
	// Check behaviour on true.
	t.Run("true", func(t *testing.T) {
		mock := mock.NewT()
		catch.Check(mock, 1+1 == 2)

		mock.CheckSucceeded(t)

		if got := mock.Text(); got != "" {
			t.Errorf("got %s", got)
		}
	})

	// Check behaviour on false.
	t.Run("false", func(t *testing.T) {
		mock := mock.NewT()
		catch.Check(mock, 1+2 == 2)

		mock.CheckFailed(t, false)

		const expected = "check failed: 1+2 == 2"
		if got := mock.Text(); got != expected {
			t.Errorf("expected %s, got %s", expected, got)
		}
	})

	// Check behaviour on false.
	// Message uses code comment.
	t.Run("false // comment", func(t *testing.T) {
		mock := mock.NewT()
		catch.Check(mock, 1+2 == 2) // My comment

		mock.CheckFailed(t, false)

		const expected = "check failed: 1+2 == 2\ncomment  : My comment"
		if got := mock.Text(); got != expected {
			t.Errorf("expected %s, got %s", expected, got)
		}
	})

	// Check behaviour on false.
	// Message uses fields
	t.Run("false // fields", func(t *testing.T) {
		mock := mock.NewT()
		field1 := "some field"
		field2 := 1234
		field3 := "\x1b1mescape\x1b0m"
		catch.Check(mock, 1+2 == 2, field1, field2, field3, 1+2)

		mock.CheckFailed(t, false)

		const expected = `check failed: 1+2 == 2
field1   : some field
field2   : 1234
field3   : "\x1b1mescape\x1b0m"
1 + 2    : 3`
		if got := mock.Text(); got != expected {
			t.Errorf("expected %s, got %s", expected, got)
		}
	})

	// Check behaviour on false.
	// Parenthesis in expression.
	t.Run("(1+1) == 3", func(t *testing.T) {
		mock := mock.NewT()
		catch.Check(mock, (1+1) == 3)

		mock.CheckFailed(t, false)

		const expected = "check failed: (1 + 1) == 3"
		if got := mock.Text(); got != expected {
			t.Errorf("expected %s, got %s", expected, got)
		}
	})

	// Check behaviour on false.
	// Parenthesis around and in expression.
	t.Run("((1 + 1) == 3)", func(t *testing.T) {
		mock := mock.NewT()
		catch.Check(mock, ((1 + 1) == 3))

		mock.CheckFailed(t, false)

		const expected = "check failed: ((1 + 1) == 3)"
		if got := mock.Text(); got != expected {
			t.Errorf("expected %s, got %s", expected, got)
		}
	})
}

func TestCheckEqual(t *testing.T) {
	// Note: Deliberately not using a table driven test here.  The structure
	// of the following tests are the same, but part of the test is to properly
	// create error messages from the source code.

	// CheckEqual behaviour on equality.
	t.Run("true", func(t *testing.T) {
		mock := mock.NewT()
		catch.CheckEqual(mock, 2, 1+1)

		mock.CheckSucceeded(t)

		if got := mock.Text(); got != "" {
			t.Errorf("got %s", got)
		}
	})

	// CheckEqual behaviour on inequality
	t.Run("false", func(t *testing.T) {
		mock := mock.NewT()
		catch.CheckEqual(mock, 2, 1+2)

		mock.CheckFailed(t, false)

		const expected = `check failed: 2 == 1 + 2
got      : 2
expected : 3`
		if got := mock.Text(); got != expected {
			t.Errorf("expected %s, got %s", expected, got)
		}
	})

	t.Run("no error", func(t *testing.T) {
		mock := mock.NewT()
		catch.CheckEqual(mock, errors.New("mock error"), nil)

		mock.CheckFailed(t, false)

		const expected = `check failed: errors.New("mock error") == nil
got      : mock error
expected : <nil>`
		if got := mock.Text(); got != expected {
			t.Errorf("expected %s, got %s", expected, got)
		}
	})

	// CheckEqual behaviour on inequality.
	// Parenthesis in left expression.
	t.Run("(1 + 1) == 3", func(t *testing.T) {
		mock := mock.NewT()
		catch.CheckEqual(mock, (1 + 1), 3)

		mock.CheckFailed(t, false)

		const expected = `check failed: (1 + 1) == 3
got      : 2
expected : 3`
		if got := mock.Text(); got != expected {
			t.Errorf("expected %s, got %s", expected, got)
		}
	})

	// CheckEqual behaviour on inequality.
	// Parenthesis in right expression.
	t.Run("2 == (1 + 2)", func(t *testing.T) {
		mock := mock.NewT()
		catch.CheckEqual(mock, 2, (1 + 2))

		mock.CheckFailed(t, false)

		const expected = `check failed: 2 == (1 + 2)
got      : 2
expected : 3`
		if got := mock.Text(); got != expected {
			t.Errorf("expected %s, got %s", expected, got)
		}
	})

	// CheckEqual behaviour on inequality.
	// Unprintable characters
	t.Run("\"\x1b[0m\" == \"\x1b[1m\"", func(t *testing.T) {
		mock := mock.NewT()
		catch.CheckEqual(mock, "\x1b[0m", "\x1b[1m")

		mock.CheckFailed(t, false)

		const expected = `check failed: "\x1b[0m" == "\x1b[1m"
got      : "\x1b[0m"
expected : "\x1b[1m"`
		if got := mock.Text(); got != expected {
			t.Errorf("expected %s, got %s", expected, got)
		}
	})

	// CheckEqual behaviour on inequality.
	// Mismatchted types
	t.Run("1 == uint(1)", func(t *testing.T) {
		mock := mock.NewT()
		catch.CheckEqual(mock, 1, uint(1))

		mock.CheckFailed(t, false)

		const expected = `check failed: 1 == uint(1)
got      : 1
expected : 1
types    : int != uint`
		if got := mock.Text(); got != expected {
			t.Errorf("expected %s, got %s", expected, got)
		}
	})
}

func TestCheckIsNil(t *testing.T) {
	// Check behaviour on true.
	t.Run("true", func(t *testing.T) {
		mock := mock.NewT()
		catch.CheckIsNil(mock, ([]byte)(nil))

		mock.CheckSucceeded(t)

		if got := mock.Text(); got != "" {
			t.Errorf("got %#v", got)
		}
	})

	// Check behaviour on false.
	t.Run("false", func(t *testing.T) {
		mock := mock.NewT()
		catch.CheckIsNil(mock, []byte{})

		mock.CheckFailed(t, false)

		const expected = "check is nil failed: []byte{}\ngot      : []"
		if got := mock.Text(); got != expected {
			t.Errorf("expected %#v, got %#v", expected, got)
		}
	})

	// Check behaviour on false.
	// Message uses code comment.
	t.Run("comment", func(t *testing.T) {
		mock := mock.NewT()
		catch.CheckIsNil(mock, []byte{}) // My comment

		mock.CheckFailed(t, false)

		const expected = "check is nil failed: []byte{}\ngot      : []\ncomment  : My comment"
		if got := mock.Text(); got != expected {
			t.Errorf("expected %#v, got %#v", expected, got)
		}
	})

	// Check behaviour on false.
	// Slice contains bytes
	t.Run("printable", func(t *testing.T) {
		mock := mock.NewT()
		catch.CheckIsNil(mock, []byte{1, 2, 3})

		mock.CheckFailed(t, false)

		const expected = "check is nil failed: []byte{1, 2, 3}\ngot      : [1 2 3]"
		if got := mock.Text(); got != expected {
			t.Errorf("expected %#v, got %#v", expected, got)
		}
	})

	// Check behaviour on false.
	// Slice contains strings.
	t.Run("unprintable", func(t *testing.T) {
		mock := mock.NewT()
		testSlice := []string{"Hello", ",", "world!"}
		catch.CheckIsNil(mock, testSlice)

		mock.CheckFailed(t, false)

		const expected = "check is nil failed: testSlice\ngot      : []string{\"Hello\", \",\", \"world!\"}"
		if got := mock.Text(); got != expected {
			t.Errorf("expected %#v, got %#v", expected, got)
		}
	})
}

func BenchmarkCheck(b *testing.B) {
	t := mock.NewT()

	for i := 0; i < b.N; i++ {
		t.Reset()
		catch.Check(t, false)
	}

	catch.CheckEqual(b, t.Text(), "check failed: false")
}

func BenchmarkCheck_comment(b *testing.B) {
	t := mock.NewT()

	for i := 0; i < b.N; i++ {
		t.Reset()
		catch.Check(t, false) // A comment for the message
	}

	catch.CheckEqual(b, t.Text(), "check failed: false\ncomment  : A comment for the message")
}

func BenchmarkCheck_fields(b *testing.B) {
	const message = "A string field"
	const value = 1234

	const want = `check failed: false
message  : A string field
value    : 1234
comment  : A comment for the message`

	t := mock.NewT()

	for i := 0; i < b.N; i++ {
		t.Reset()
		catch.Check(t, false, message, value) // A comment for the message
	}

	catch.CheckEqual(b, t.Text(), want)
}
