package catch

import (
	"reflect"
	"strings"
)

// Check does nothing if 'value' is true, but will a report an error if it is
// false.  The error is reported using Errorf, meaning that the error will be
// logged and the test will be marked as failing.
func Check(t TB, value bool, fields ...interface{}) {
	// If the check passes, there is nothing to report
	if value {
		return
	}

	// Skip this function when logging the location of the error.
	t.Helper()

	// Load the call expression that failed.
	expr, comment, err := loadExpressionFromCaller()
	must(t, err)

	// Format the message.
	expr1, rest := splitExpression1(expr)
	t.Errorf("check failed: %s%s%s", expr1,
		formatFields(rest, fields),
		formatComment(comment))
}

// CheckEqual does nothing if 'expected' and 'got' are equal, but will a report
// an error otherwise.  The error is reported using Errorf, meaning that the
// error will be logged and the test will be marked as failing.
func CheckEqual(t TB, got, expected interface{}, fields ...interface{}) {
	// If the check passes, there is nothing to report
	if reflect.DeepEqual(got, expected) {
		return
	}

	// Skip this function when logging the location of the error.
	t.Helper()

	const msg = `check failed: %s == %s
got      : %v
expected : %v%s%s%s`

	// Load the call expression that failed.
	expr, comment, err := loadExpressionFromCaller()
	must(t, err)

	// Format the message.
	expr1, expr2, rest := splitExpression2(expr)
	if isPrintable(got) && isPrintable(expected) {
		t.Errorf(msg, expr1, expr2, got, expected,
			formatTypes(got, expected),
			formatFields(rest, fields),
			formatComment(comment))
	} else {
		t.Errorf(strings.Replace(msg, "%v", "%#v", -1),
			expr1, expr2, got, expected,
			formatTypes(got, expected),
			formatFields(rest, fields),
			formatComment(comment))
	}
}

// CheckIsNil does nothing if 'got' is a nil chan, func, map,
// pointer, or slice value, but will a report an error otherwise.  The error is
// reported using Errorf, meaning that the error will be logged and the test
// will be marked as failing.
//
// This function can only be called with chan, func, map, pointer, or slice
// values.  It will panic otherwise.
func CheckIsNil(t TB, got interface{}, fields ...interface{}) {
	// If the check passes, there is nothing to report.
	if reflect.ValueOf(got).IsNil() {
		return
	}

	// Skip this function when logging the location of the error.
	t.Helper()

	const msg = `check is nil failed: %s
got      : %v%s%s`

	// Load the call expression that failed.
	expr, comment, err := loadExpressionFromCaller()
	must(t, err)

	// Format the message.
	expr1, rest := splitExpression1(expr)
	if isPrintable(got) {
		t.Errorf(msg, expr1, got,
			formatFields(rest, fields),
			formatComment(comment))
	} else {
		t.Errorf(strings.Replace(msg, "%v", "%#v", -1),
			expr1, got,
			formatFields(rest, fields),
			formatComment(comment))
	}
}
