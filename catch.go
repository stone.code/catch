// Package catch provides utility functions that work with the standard
// library's testing capabilities. The functions encapsulate standard tests, and
// standardise logging for failures.  However, package catch does not output
// anything directly.  All messages are formatted using the standard methods on
// the test object.
//
// As an example, when the following test fails:
//
//	func TestFancyCalculation(t *testing.T) {
//	    fancyCalculation := 1 + 1
//	    catch.CheckEqual(t, fancyCalculation, 1)
//	}
//
// The log will be:
//
//	testing_test.go:285: check failed: fancyCalculation == 1
//	        got      : 2
//	        expected : 1
//
// The source files for the test code should be available and unmodified at
// their original locations when the tests are run.  The source code is read and
// partially parsed to create messages for any failed tests.  If the source code
// is not available, tests can still be run, but the messages for any failed
// tests will not be complete.
//
// # Message Formatting and Annotation
//
// When a test fails, package catch will create an error message based on the
// user's source code.  The source code for the helper's caller will be
// opened, and the expression for the arguments included in the error report.
//
// If the line with error contains a line comment, the line comment will also be
// included in the error message.
//
// User's can also add an arbitrary number of arguments to any call.  Those
// arguments are not used for the test, but will be included in the message if
// the test fails.  With this facility, users can easily add important context
// to failing tests.
package catch

import (
	"bytes"
	"fmt"
	"reflect"
	"strings"
	"unicode"

	"gitlab.com/stone.code/catch/internal/convert"
)

// TB is a subset of the interface testing.TB.  These methods are the portion
// of that interface required for the checks.
type TB interface {
	Errorf(format string, args ...interface{})
	Fatalf(format string, args ...interface{})
	Logf(format string, args ...interface{})
	Helper()
}

// must ensures that we can proceed with logging an error, even if there was
// an error while loading and parsing to find the expression.
func must(t TB, err error) {
	if err != nil {
		t.Logf("%s", err)
	}
}

// isPrintable returns true if the value can be formatted with %v without
// sending unprintable characters to the screen.  Also used to trigger
// printing with %#v when too much information would otherwise be lost.
func isPrintable(v interface{}) bool {
	// Nil values are printable.
	if v == nil {
		return true
	}

	// Check that all of the runes in the string are printable.
	if s, ok := convert.AsString(v); ok {
		return strings.IndexFunc(s, func(r rune) bool {
			return !unicode.IsPrint(r)
		}) < 0
	}

	if kind := reflect.TypeOf(v).Kind(); kind <= reflect.Complex128 {
		// Bool, ints, uints, floats, and complex numbers.
		return true
	} else if (kind == reflect.Slice || kind == reflect.Array) && reflect.TypeOf(v).Elem().Kind() <= reflect.Complex128 {
		// Slices and arrays of bools, ints, uints, floats, or complex numbers.
		return true
	}

	return false
}

func formatTypes(got, expected interface{}) string {
	if got == nil || expected == nil || reflect.TypeOf(got) == reflect.TypeOf(expected) {
		return ""
	}

	return fmt.Sprintf("\ntypes    : %T != %T", got, expected)
}

func formatFields(fields []string, values []interface{}) string {
	// If there is an error loading the call expression, there can be a mismatch
	// between the number of arg expressions and the number of values.  Even if
	// some arg expressions are present, they are likely erroneous.  Want to
	// avoid any panics (index out-of-bounds), but nevertheless print as much
	// information as possible.
	if len(fields) != len(values) {
		fields = make([]string, len(values))
	}

	w := bytes.NewBuffer(nil)
	for i, v := range fields {
		if isPrintable(values[i]) {
			fmt.Fprintf(w, "\n%-9s: %v", v, values[i])
		} else {
			fmt.Fprintf(w, "\n%-9s: %#v", v, values[i])
		}
	}

	return w.String()
}

func formatComment(text string) string {
	if text == "" {
		return ""
	}

	return "\ncomment  : " + text
}
